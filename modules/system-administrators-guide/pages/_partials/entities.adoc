:BOOKID: system-administrators-guide
:BZURL: link:https://pagure.io/fedora-docs/system-administrators-guide/issues[https://pagure.io/]
:HOLDER: Red Hat, Inc. and others
:MAJOROS: Fedora
:MAJOROSVER: Fedora Rawhide
:OSORG: The Fedora Project
:PKGOS: fc35
:PRODUCT: Fedora Documentation
:PRODVER: Rawhide
:YEAR: 2021
