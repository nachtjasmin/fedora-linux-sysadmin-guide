:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Opening_GUI_Applications]]
= Opening Graphical Applications

Fedora provides graphical applications in addition to command line utilities for configuring many features. This chapter describes methods for opening `Graphical User Interface`, or _GUI_, applications in various environments.

[[gui-from_cli]]
== Opening graphical applications from the command line

Graphical applications can be launched from a terminal window or console session by simply typing the name of the application.

[subs="quotes, macros"]
----

[fedorauser@localhost]$ [command]#firefox#
----

.File names vs Application names
[NOTE]
====

Programs are opened from the command line using the name of the executable file provided in the program's package. An entry in the desktop menu will often be named differently from the file it executes. For example, the GNOME disk management utility appears in the menu as [application]*Disks*, and the file it executes is `/usr/bin/gnome-disks`.

====

When a program is executed on the command line, the terminal is occupied until the program completes. When a graphical application is executed from the command line, the program's error output, or `STDERR`, is sent to the terminal window. This can be especially useful when troubleshooting.

.Viewing errors by launching graphical applications from the command line
====

----

[fedorauser@localhost]$ astromenace-wrapper
	AstroMenace 1.3.1 121212

	Open XML file: /home/fedorauser/.config/astromenace/amconfig.xml
	VFS file was opened /usr/share/astromenace/gamedata.vfs

	Vendor     : OpenAL Community
	Renderer   : OpenAL Soft
	Version    : 1.1 ALSOFT 1.15.1
	ALut ver   : 1.1

	Font initialized: DATA/FONT/LiberationMono-Bold.ttf

	Current Video Mode: 3200x1080 32bit

	Xinerama/TwinView detected.
	Screen count: 2
	Screen #0: (0, 0) x (1920, 1080)
	Screen #1: (1920, 0) x (1280, 1024)

	Supported resolutions list:
	640x480 16bit
	640x480 32bit
	640x480 0bit
	768x480 16bit
	<output truncated>
----

====

To launch a graphical application, but fork the additional output into the background and return the terminal for immediate use, use the shell's `job control` feature.

[subs="quotes, macros"]
----

[fedorauser@localhost]$ [command]#emacs foo.txt &#
----

.Ending a session
[IMPORTANT]
====

Applications that hold the command line prompt until they complete will close when the terminal session ends, even if they are forked into the background.

====

GUI programs can also be launched on one `TTY` and displayed on another by specifying the `DISPLAY` variable. This can be useful when running multiple graphical sessions, or for troubleshooting problems with a desktop session.

. Switch to another TTY using the key combination kbd:[Ctrl + Alt + F2] and log in. Note that consoles are available by default with kbd:[F2] through kbd:[F6].

. Identify the X session you want to target. The `DISPLAY` variable is always an integer preceded by a colon, and will be *:0* in most cases. Check the arguments of the currently running [application]*X* process to verify the value. The command below shows both the `DISPLAY` variable as well as the TTY that [application]*X* is running on, `tty1`.
+
[subs="macros"]
----

[fedorauser@localhost]$ ps aux|grep /usr/bin/X
root      1498  7.1  1.0 521396 353984 pass:quotes[`tty1`]    Ss+  00:04  66:34 /usr/bin/X pass:quotes[`:0`] vt1 -background none -nolisten tcp -auth /var/run/kdm/A:0-22Degc

root     23874  0.0  0.0 109184   900 pts/21   S+   15:35   0:00 grep --color=auto /usr/bin/X
----

. Specify the `DISPLAY` variable when executing the program.
+
[subs="quotes, macros"]
----

[fedorauser@localhost]$ [command]#DISPLAY=:0 gnome-shell --replace &#
----

. Switch back to the TTY the graphical session is running on. Since the example above shows [application]*X* running on `vt1`, pressing kbd:[Ctrl + Alt + F1] will return to the desktop environment.

[[gui-alt_f2]]
== Launching Applications with kbd:[Alt + F2]

Most desktop environments follow the convention of using the key combination kbd:[Alt + F2] for opening new applications. Pressing kbd:[Alt + F2] brings up a prompt for a command to be entered into.

Commands entered into this dialog box function much as they would if entered in a terminal. Applications are known by their file name, and can accept arguments.

[[fig-alt_f2-gnome]]
.Using kbd:[Alt + F2] with [application]*GNOME*

image::alt-f2_GNOME.png[GNOME command entry dialog box]

[[fig-alt-f2_kde]]
.Using kbd:[Alt + F2] with [application]*KDE*

image::alt-f2_KDE.png[KDE command entry dialog box, which also searches menu items, command history, and open applications.]

[[fig-alt-f2_lxde]]
.Using kbd:[Alt + F2] with [application]*LXDE*

image::alt-f2_LXDE.png[LXDE command entry dialog box.]

[[fig-alt-f2_mate]]
.Using kbd:[Alt + F2] with [application]*MATE*

image::alt-f2_MATE.png[MATE command entry dialog box.]

[[fig-alt-f2_xfce]]
.Using kbd:[Alt + F2] with [application]*XFCE*

image::alt-f2_XFCE.png[XFCE command entry dialog box.]

[[gui-from_menu]]
== Launching applications from the Desktop Menu

Applications can also be opened from the menu system provided by the desktop environment in use. While the presentation may vary between desktop environments, the menu entries and their categories are provided by the individual application and standardized by the link:++https://standards.freedesktop.org/menu-spec/menu-spec-latest.html++[freedesktop.org Desktop Menu Specification]. Some desktop environments also provide search functionality in their menu system to allow quick and easy access to applications.

[[gui-from_menu-gnome]]
=== Using GNOME menus

The GNOME menu, called the `overview`, can be accessed by either clicking the `Activities` button in the top left of the primary display, by moving the mouse past the top left `hot corner`, or by pressing the kbd:[Super] ( kbd:[Windows] ) key. The `overview` presents documents in addition to applications.

Selecting an item from the menu is best accomplished using the `search box`. Simply bring up the `overview`, and begin typing the name of the application you want to launch. Pressing enter will launch the highlighted application, or you can use the arrow keys or mouse to choose an alternative.

[[fig-searchmenu-gnome]]
.Using the GNOME search box

image::searchmenu_GNOME.png[Typing the name of an application into the overview search box will display matching menu entries. The search also matches descriptions, so that typing browser will display installed browsers.]

The `overview` can also be browsed. The bar on the left, called the `dash`, shows frequently used applications and a grid icon. Clicking on the grid icon brings up a grid in the center of the window that displays frequently used applications. The grid will display all available applications if selected using the `All` button at the bottom of the screen.

[[fig-menu-gnome]]
.Browsing GNOME menu entries

image::menu_GNOME.png[The GNOME menu has a bar on the left for frequently used applications, which includes a grid icon that brings up a grid in the center of the window. Users can then use the buttons at the bottom of the screen to display either a larger list of frequently used applications, or to view all available applications.]

To learn more about using [application]*GNOME shell*, visit link:++https://wiki.gnome.org/GnomeShell/CheatSheet++[]

[[gui-from_menu-kde]]
=== Using KDE menus

The KDE menu is opened by clicking the {MAJOROS} button at the bottom left corner of the screen. The menu initially displays favorite applications, which can be added to by right clicking any menu entry. Hovering over the icons in the lower portion of the menu will display applications, file systems, recently used applications, or options for logging out of the system.

[[fig-menu_kde]]
.The KDE desktop menu.

image::menu_KDE.png[The KDE menu displays applications in categories. The contents of the categories are displayed when clicked.]

Search functionality is also available in the KDE menu system. To search for applications, open the menu and begin typing. The menu will display matching entries.

[[fig-searchmenu_kde]]
.Searching with the KDE menu.

image::searchmenu_KDE.png[The KDE menu will search for matching applications if you type into the search box. For example, typing browser will display installed browsers and other matching entries.]

=== Using menus in LXDE, MATE, and XFCE

Menus in LXDE, MATE, and XFCE have a varied appearance but a very similar structure. They categorize applications, and the contents of a category are displayed by hovering the cursor over the entry. Applications are launched by clicking on an entry.

[[fig-menu_lxde]]
.The LXDE menu

image::menu_LXDE.png[LXDE Menu]

[[fig-menu_mate]]
.MATE menu

image::menu_MATE.png[MATE menu]

[[fig-menu_xfce]]
.XFCE Menu

image::menu_XFCE.png[XFCE Menu]
